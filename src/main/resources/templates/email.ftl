<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Spring Boot Email using FreeMarker</title>
</head>
<body>
<div style="margin-top: 10px">Greetings, ${firstName}</div>
<div>Your username is <b>${lastName}</b>

</div>
<div><button style="background-color: aqua"> <a href="http://localhost:8080/api/auth/verify?email=${email}&code=${code}">Tasdiqlash</a></button></div>
<div><button style="background-color: red"> <a href="http://localhost:8080/api/auth/cancelVerification?email=${email}">Bekor qilish</a></button></div>

<br/>
<div> Have a nice day..!</div>
</body>
</html>