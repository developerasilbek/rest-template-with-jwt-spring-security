package com.example.spring_security_demo.components;

import com.example.spring_security_demo.entity.Huquq;
import com.example.spring_security_demo.entity.Role;
import com.example.spring_security_demo.entity.User;
import com.example.spring_security_demo.enums.HuquqName;
import com.example.spring_security_demo.enums.RoleName;
import com.example.spring_security_demo.repo.HuquqRepo;
import com.example.spring_security_demo.repo.RoleRepo;
import com.example.spring_security_demo.repo.UserRepo;
import com.sun.jdi.request.StepRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Component
public class DataLoader implements CommandLineRunner {

    @Value("${spring.datasource.initialization-mode}")
    private String mode;

    @Autowired
    UserRepo userRepo;

    @Autowired
    RoleRepo roleRepo;

    @Autowired
    HuquqRepo huquqRepo;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public void run(String... args) throws Exception {
        if (mode.equals("always")){
            Set<Role> rollar = new HashSet<>();
            Set<Huquq> huquqlar = new HashSet<>();
            RoleName[] values = RoleName.values();
            for (RoleName value : values) {
                rollar.add(roleRepo.save(new Role(value)));
            }

            HuquqName[] values1 = HuquqName.values();
            for (HuquqName huquqName : values1) {
                huquqlar.add(huquqRepo.save(new Huquq(huquqName)));
            }
            User projectOwner = new User();
            projectOwner.setLogin("admin");
            projectOwner.setParoli(passwordEncoder.encode("admin"));
            projectOwner.setHuquqlari(huquqlar);
            projectOwner.setRoles(rollar);
            projectOwner.setPhoneNumber("+99899XXXXXXX");
            projectOwner.setEmail("XXX@gmail.com");
            projectOwner.setFio("Future Corporation");
            userRepo.save(projectOwner);
        }
    }
}
