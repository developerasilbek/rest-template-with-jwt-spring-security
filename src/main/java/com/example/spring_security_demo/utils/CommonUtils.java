package com.example.spring_security_demo.utils;

import java.util.Random;

public class CommonUtils {

    public static boolean confirmPassword(String password, String rePassword){
        return password.equals(rePassword);
    }

    public static int generateCode() {
        return new Random().nextInt(899999) + 100000;
    }
}
