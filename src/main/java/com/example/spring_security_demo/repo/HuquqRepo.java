package com.example.spring_security_demo.repo;

import com.example.spring_security_demo.entity.Huquq;
import com.example.spring_security_demo.enums.HuquqName;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HuquqRepo extends JpaRepository<Huquq,Long> {
    Huquq findByHuquqName(HuquqName huquqName);
}
