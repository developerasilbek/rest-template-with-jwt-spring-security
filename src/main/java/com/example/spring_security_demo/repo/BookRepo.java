package com.example.spring_security_demo.repo;

import com.example.spring_security_demo.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookRepo extends JpaRepository<Book,Long> {
}
