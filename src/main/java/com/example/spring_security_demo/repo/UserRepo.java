package com.example.spring_security_demo.repo;

import com.example.spring_security_demo.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepo extends JpaRepository<User,Long> {
    User findByLoginOrPhoneNumberOrEmail(String login, String phoneNumber, String email);
    Optional<User> findByPhoneNumber(String phoneNumber);
    Optional<User> findByEmail(String email);
    Optional<User> findByLogin(String login);
}
