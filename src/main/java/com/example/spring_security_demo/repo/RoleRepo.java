package com.example.spring_security_demo.repo;

import com.example.spring_security_demo.entity.Role;
import com.example.spring_security_demo.enums.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepo extends JpaRepository<Role,Long> {
    Role findByRoleName(RoleName roleName);
}
