package com.example.spring_security_demo.repo;

import com.example.spring_security_demo.entity.User;
import com.example.spring_security_demo.entity.Verification;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface VerificationRepo extends JpaRepository<Verification, Long> {
Optional<Verification> findByUser(User user);
Verification findByUserAndCodeAndVerifiedFalse(User user, int code);
}
