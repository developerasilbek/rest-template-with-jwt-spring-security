package com.example.spring_security_demo.controller;

import com.example.spring_security_demo.payload.ApiResponse;
import com.example.spring_security_demo.payload.SignIn;
import com.example.spring_security_demo.payload.SignUp;
import com.example.spring_security_demo.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    AuthService authService;

    @PostMapping("/login")
    public HttpEntity<?> login(@RequestBody SignIn signIn) throws Exception {
        ApiResponse response = authService.login(signIn);
        return ResponseEntity.ok(response);
    }

    @PostMapping("/register")
    public HttpEntity<?> register(@RequestBody @Valid SignUp signUp) throws Exception {
        ApiResponse response = authService.register(signUp);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/verify")
    public HttpEntity<?> verify(@RequestParam String email, @RequestParam String code) throws Exception {
        ApiResponse response = authService.verify(email, code);
        return ResponseEntity.status(response.isSuccess() ? 200 : 409).body(response);
    }
    @GetMapping("/verifyPhone")
    public HttpEntity<?> verifyPhone(@RequestParam String phone, @RequestParam int code) throws Exception {
        ApiResponse response = authService.verifyPhone(phone, code);
        return ResponseEntity.status(response.isSuccess() ? 200 : 409).body(response);
    }
    @GetMapping("/cancelVerification")
    public HttpEntity<?> verify(@RequestParam String email) throws Exception {
        ApiResponse response = authService.cancel(email);
        return ResponseEntity.status(response.isSuccess() ? 200 : 409).body(response);
    }


}
