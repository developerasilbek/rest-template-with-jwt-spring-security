package com.example.spring_security_demo.controller;

import com.example.spring_security_demo.payload.ApiResponse;
import com.example.spring_security_demo.payload.BookDto;
import com.example.spring_security_demo.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/book")
public class BookController {

    @Autowired
    BookService bookService;

    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN','ADD_BOOK')")
    @PostMapping("/save")
    public HttpEntity<?> save(@RequestBody BookDto bookDto){
        ApiResponse response = bookService.save(bookDto);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/all")
    public HttpEntity<?> all(){
        ApiResponse response = bookService.all();
        return ResponseEntity.ok(response);
    }
}
