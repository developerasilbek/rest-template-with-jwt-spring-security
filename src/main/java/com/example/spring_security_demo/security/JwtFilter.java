package com.example.spring_security_demo.security;

import com.example.spring_security_demo.entity.User;
import com.example.spring_security_demo.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;


public class JwtFilter extends OncePerRequestFilter {
    @Autowired
    JwtTokenProvider provider;

    @Autowired
    UserRepo userRepo;



    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        User user = getUserFromToken(httpServletRequest);
        if (user !=null){
            if (user.isEnable() &&
            user.isCredentialsNonExpired()
            && user.isAccountNonLocked()
            && user.isAccountNonExpired()){
                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                       user,
                       null,
                       user.getAuthorities()
                );
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        }
        filterChain.doFilter(httpServletRequest,httpServletResponse);
    }

    public String getTokenFromRequest(HttpServletRequest request){
        String token = request.getHeader("Authorization");
        return token!=null? token.substring(7) : null;
    }
    public User getUserFromToken(HttpServletRequest request){
        String token = getTokenFromRequest(request);
        if (token!=null && provider.validateToken(token)){
            Long userId = provider.getSubjectFromToken(token);

            return userRepo.findById(userId).orElse(null);
        }
        return null;
    }
}
