package com.example.spring_security_demo.security;

import com.example.spring_security_demo.entity.User;
import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class JwtTokenProvider {

    @Value("${jwt.secretKey}")
    private String secretKey;

    @Value("${jwt.expireDateInMilliSeconds}")
    private Long expireDateInMilliSeconds;


    public String generateToken(User user){
        return Jwts.builder()
                .setSubject(user.getId().toString())
                .claim("authorities",user.getAuthorities())
                .setIssuedAt(new Date())
                .setExpiration(new Date(new Date().getTime()+expireDateInMilliSeconds))
                .signWith(SignatureAlgorithm.HS512,secretKey)
                .compact();
    }

    public boolean validateToken(String token) {
        try {
            Jwts
                    .parser()
                    .setSigningKey(secretKey)
                    .parseClaimsJws(token);
            return true;
        } catch (ExpiredJwtException e) {
            System.err.println("Muddati o'tgan");
        } catch (MalformedJwtException malformedJwtException) {
            System.err.println("Buzilgan token");
        } catch (SignatureException s) {
            System.err.println("Kalit so'z xato");
        } catch (UnsupportedJwtException unsupportedJwtException) {
            System.err.println("Qo'llanilmagan token");
        } catch (IllegalArgumentException ex) {
            System.err.println("Bo'sh token");
        }
        return false;
    }

    public Long getSubjectFromToken(String token){
        try {
            String subject = Jwts
                    .parser()
                    .setSigningKey(secretKey)
                    .parseClaimsJws(token)
                    .getBody()
                    .getSubject();
            return Long.parseLong(subject);
        }catch (Exception e){
            System.out.println("Error");
        }
        return null;
    }

}
