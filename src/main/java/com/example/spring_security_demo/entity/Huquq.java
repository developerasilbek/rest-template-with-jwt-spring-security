package com.example.spring_security_demo.entity;

import com.example.spring_security_demo.enums.HuquqName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Huquq implements GrantedAuthority {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false,unique = true)
    private HuquqName huquqName;

    public Huquq(HuquqName huquqName) {
        this.huquqName = huquqName;
    }

    @Override
    public String getAuthority() {
        return huquqName.name();
    }
}
