package com.example.spring_security_demo.mapper;

import com.example.spring_security_demo.entity.Book;
import com.example.spring_security_demo.payload.BookDto;

public class BookMapper {
    public static Book bookFromDto(BookDto dto) {
        Book book = new Book();
        if (dto.getId()!=null){
            book.setId(dto.getId());
        }
        book.setName(dto.getName());
        book.setAuthor(dto.getAuthor());
        book.setPrice(dto.getPrice());
        return book;
    }

    public static BookDto dtoFromBook(Book book) {
        return new BookDto(book.getId(), book.getName(), book.getAuthor(), book.getPrice());
    }
}
