package com.example.spring_security_demo.service;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Service
public class VerificationService {
    private final static String ACCOUNT_SID = " ACb98618dc7f3c77e485d9cd5b2852af49";
    private final static String AUTH_TOKEN = "6057bae106112a8f8ac6d7ac93e2d14d";
    private final static String SENDER_PHONE = "+16573857309";

    @Autowired
    private JavaMailSender sender;

    @Autowired
    private Configuration configuration;

    public boolean sendEmailOrSMS(String emailOrPhone, int code, boolean isEmail) throws MessagingException, IOException, TemplateException {
       try {
           if (isEmail) {
               MimeMessage mimeMessage = sender.createMimeMessage();
               MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);
               helper.setSubject("Verification by LCMS");
               helper.setTo(emailOrPhone);
               Map<String, Object> model = new HashMap<>();
               model.put("firstName", "Shoxzod");
               model.put("lastName", "Ravshanov");
               model.put("email", emailOrPhone);
               model.put("code", String.valueOf(code));
               Template template = configuration.getTemplate("email.ftl");
               String html = FreeMarkerTemplateUtils.processTemplateIntoString(template, model);
               helper.setText(html, true);
               sender.send(mimeMessage);
               return true;
           } else {
               Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
               Message message = Message.creator(
                               new com.twilio.type.PhoneNumber(emailOrPhone),
                               new com.twilio.type.PhoneNumber(SENDER_PHONE),
                               "Your code is " + code + ". Don't share this code!!!"
                       )
                       .create();
               System.out.println(message.getSid());
               return true;
           }
       } catch (Exception e){
           e.printStackTrace();
           return false;
       }
    }

}
