package com.example.spring_security_demo.service;

import com.example.spring_security_demo.mapper.BookMapper;
import com.example.spring_security_demo.payload.ApiResponse;
import com.example.spring_security_demo.payload.BookDto;
import com.example.spring_security_demo.repo.BookRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class BookService {
    @Autowired
    BookRepo bookRepo;

    public ApiResponse save(BookDto bookDto) {
        bookRepo.save(BookMapper.bookFromDto(bookDto));
        return new ApiResponse("Saved",true);
    }

    public ApiResponse all() {
        return new ApiResponse("All",true,
                bookRepo.findAll()
                        .stream()
                        .map(BookMapper::dtoFromBook)
                        .collect(Collectors.toList()));
    }
}
