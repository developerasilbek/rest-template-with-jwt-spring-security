package com.example.spring_security_demo.service;

import com.example.spring_security_demo.entity.User;
import com.example.spring_security_demo.entity.Verification;
import com.example.spring_security_demo.enums.HuquqName;
import com.example.spring_security_demo.enums.RoleName;
import com.example.spring_security_demo.payload.ApiResponse;
import com.example.spring_security_demo.payload.ResToken;
import com.example.spring_security_demo.payload.SignIn;
import com.example.spring_security_demo.payload.SignUp;
import com.example.spring_security_demo.repo.HuquqRepo;
import com.example.spring_security_demo.repo.RoleRepo;
import com.example.spring_security_demo.repo.UserRepo;
import com.example.spring_security_demo.repo.VerificationRepo;
import com.example.spring_security_demo.security.JwtTokenProvider;
import com.example.spring_security_demo.security.MyUserDetailsService;
import com.example.spring_security_demo.utils.CommonUtils;
import freemarker.template.TemplateException;
import javassist.tools.web.BadHttpRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

@Service
public class AuthService {
    @Autowired
    VerificationService verificationService;

    @Autowired
    UserRepo userRepo;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    MyUserDetailsService myUserDetailsService;

    @Autowired
    AuthenticationManager manager;

    @Autowired
    JwtTokenProvider provider;

    @Autowired
    RoleRepo roleRepo;

    @Autowired
    HuquqRepo huquqRepo;

    @Autowired
    VerificationRepo verificationRepo;

    public ApiResponse login(SignIn signIn) throws Exception {
//        User user = checkUser(signIn);
        Authentication authenticate = manager.authenticate(new UsernamePasswordAuthenticationToken(signIn.getUsername(), signIn.getPassword()));

        Optional<User> optionalByLogin = userRepo.findByLogin(signIn.getUsername());
        if (optionalByLogin.isPresent() && optionalByLogin.get().isEnable()) {
            String token = provider.generateToken((User) authenticate.getPrincipal());
            return new ApiResponse("ok", true, new ResToken(token));
        } else {
            Optional<User> optionalByEmail = userRepo.findByEmail(signIn.getUsername());
            if (optionalByEmail.isPresent() && optionalByEmail.get().isEnable()) {

                String token = provider.generateToken((User) authenticate.getPrincipal());
                return new ApiResponse("ok", true, new ResToken(token));
            } else {
                Optional<User> optionalByPhoneNumber = userRepo.findByPhoneNumber(signIn.getUsername());
                if (optionalByPhoneNumber.isPresent()) {
                    int i = CommonUtils.generateCode();
                    if (verificationService.sendEmailOrSMS(signIn.getUsername(), i, false)) {
                        Optional<Verification> byUser = verificationRepo.findByUser(optionalByPhoneNumber.get());
                        Verification verification = new Verification();
                        if (byUser.isPresent()) {
                            verification = byUser.get();
                        } else {
                            verification.setUser(optionalByPhoneNumber.get());
                        }
                        verification.setCode(i);
                        verification.setVerified(false);
                        verificationRepo.save(verification);
                        return new ApiResponse("Code is sent to your number. Please, confirm!!!", true);
                    }
                }
                return new ApiResponse("Error", false);
            }
        }
    }

    public User checkUser(SignIn signIn) throws Exception {
        User user = (User) myUserDetailsService.loadUserByUsername(signIn.getUsername());
        if (passwordEncoder.matches(signIn.getPassword(), user.getPassword())) {
            if (user.isAccountNonExpired()) {
                if (user.isAccountNonLocked()) {
                    if (user.isCredentialsNonExpired()) {
                        if (user.isEnable()) {
                            return user;
                        }
                        throw new Exception("Account is not enable");
                    }
                    throw new Exception("Account credential is expired");
                }
                throw new Exception("Account is locked");
            }
            throw new Exception("Account expired");
        } else {
            throw new Exception("Parol nito shekilli");
        }

    }

    @Transactional
    public ApiResponse register(SignUp signUp) throws BadHttpRequest, MessagingException, TemplateException, IOException {
        if (CommonUtils.confirmPassword(signUp.getPassword(), signUp.getRePassword())) {
            User user = new User();
            user.setFio(signUp.getFio());
            user.setPhoneNumber(signUp.getPhoneNumber());
            user.setEmail(signUp.getEmail());
            user.setParoli(passwordEncoder.encode(signUp.getPassword()));
            user.setLogin(signUp.getLogin());
            user.setEnable(false);
            user.setRoles(new HashSet<>(List.of(roleRepo.findByRoleName(RoleName.ROLE_STUDENT))));
            user.setHuquqlari(new HashSet<>(List.of(huquqRepo.findByHuquqName(HuquqName.GET_BOOK))));
            user = userRepo.save(user);
            int i = CommonUtils.generateCode();
            boolean b = verificationService.sendEmailOrSMS(signUp.getEmail(), i, true);
            if (b) {
                Verification verification = new Verification();
                verification.setUser(user);
                verification.setCode(i);
                verificationRepo.save(verification);
                return new ApiResponse("Verify via email", true);
            }
        }
        throw new BadHttpRequest(new Exception("Error"));
    }

    @Transactional
    public ApiResponse verify(String email, String code) {
        User user = userRepo.findByEmail(email).orElseThrow(() -> new IllegalArgumentException("Wrong email"));
        Verification verification = verificationRepo.findByUserAndCodeAndVerifiedFalse(user, Integer.parseInt(code));
        user.setEnable(true);
        userRepo.save(user);
        verification.setVerified(true);
        verificationRepo.save(verification);
        return new ApiResponse("success", true);
    }

    public ApiResponse cancel(String email) {
        User user = userRepo.findByEmail(email).orElseThrow(() -> new IllegalArgumentException("Wrong email"));
        if(!user.isEnable()){
            userRepo.delete(user);
        }
        return new ApiResponse("ok", true);
    }

    public ApiResponse verifyPhone(String phone, int code) {
        User user = userRepo.findByPhoneNumber(phone).orElseThrow(() -> new IllegalArgumentException("Wrong email"));
        Verification verification = verificationRepo.findByUserAndCodeAndVerifiedFalse(user, code);
        if(user.isEnable()){
            verification.setVerified(true);
            verificationRepo.save(verification);
            return new ApiResponse("success", true);
        }
        return new ApiResponse("User isn't verified by email", false);
    }
}
