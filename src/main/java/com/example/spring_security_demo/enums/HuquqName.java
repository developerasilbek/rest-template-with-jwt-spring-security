package com.example.spring_security_demo.enums;

public enum HuquqName {
    ADD_BOOK,
    DELETE_BOOK,
    EDIT_BOOK,
    GET_BOOK
}
