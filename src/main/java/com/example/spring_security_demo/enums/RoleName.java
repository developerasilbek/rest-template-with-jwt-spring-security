package com.example.spring_security_demo.enums;

public enum RoleName {
    ROLE_ADMIN,
    ROLE_STUDENT
}
